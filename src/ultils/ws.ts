import socketIOClient from "socket.io-client";

export const WS = "https://webrtc.staging-demo.net/";
export const socket = socketIOClient(WS);

export const signalPeerData = (data: any) => {
  socket.emit("conn-signal", data);
};

export const createNewRoom = (identity: string) => {
  // emit an event to server that we would like to create new room
  const data = {
    identity,
  };

  socket.emit("create-new-room", data);
};

export const joinRoom = (identity: string, roomId: string,isMobile:boolean) => {
  //emit an event to server that we would to join a room
  const data = {
    roomId,
    identity,
    isMobile
  };

  socket.emit("join-room", data);
};

export function detectMob() {
  const toMatch = [
      /Android/i,
      /webOS/i,
      /iPhone/i,
      /iPad/i,
      /iPod/i,
      /BlackBerry/i,
      /Windows Phone/i
  ];
  
  return toMatch.some((toMatchItem) => {
      return window.navigator.userAgent.match(toMatchItem);
  });
}