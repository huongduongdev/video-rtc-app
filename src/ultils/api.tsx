import axios from "axios";
const serverApi = "https://webrtc.staging-demo.net/api";

export const getRoomExists = async (roomId:string) => {
    const response = await axios.get(`${serverApi}/room-exists/${roomId}`);
    return response.data;
  };