import React from "react";
import { Button } from "../../components/Button";
import { BsFillRecord2Fill } from "react-icons/bs";
import clsx from "clsx";
interface IstartRecordingProps {
  stopRecording: () => void;
  startRecording: () => void;
  recorder: any;
}
const Record = ({
  stopRecording,
  startRecording,
  recorder,
}: IstartRecordingProps) => {
  const handleRecordFunc = recorder ? stopRecording : startRecording;
  return (
    <Button
      className={clsx("btn-video", {
        ["active"]: recorder,
      })}
      onClick={handleRecordFunc}
    >
      <BsFillRecord2Fill />
    </Button>
  );
};

export default Record;
