import React from "react";
import { Button } from "../../components/Button";
import { MdSettingsVoice } from "react-icons/md";
import clsx from "clsx";
interface IstartRecordingProps {
  stopRecording: () => void;
  startRecording: () => void;
  recorder: any;
}
const RecordAudio = ({
  stopRecording,
  startRecording,
  recorder,
}: IstartRecordingProps) => {
  const handleRecordFunc = recorder ? stopRecording : startRecording;
  return (
    <Button
      className={clsx("btn-video", {
        ["active"]: recorder,
      })}
      onClick={handleRecordFunc}
    >
      <MdSettingsVoice />
    </Button>
  );
};

export default RecordAudio;
