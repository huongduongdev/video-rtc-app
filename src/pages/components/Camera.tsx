import React, { useState } from "react";
import { FaVideoSlash, FaVideo } from "react-icons/fa";
import { Button } from "../../components/Button";
import clsx from "clsx";
const Camera: React.FC<{
  localStream: MediaStream;
}> = ({ localStream }) => {
  const [cameraEnabled, setCameraEnabled] = useState(true);

  const handleToggleCamera = () => {
    localStream
      .getVideoTracks()
      .forEach((track) => (track.enabled = !track.enabled));
    setCameraEnabled(!cameraEnabled);
  };

  return (
    <Button
      className={clsx("btn-video", {
        ["active"]: cameraEnabled,
      })}
      onClick={handleToggleCamera}
    >
      {cameraEnabled ? <FaVideo /> : <FaVideoSlash />}
    </Button>
  );
};

export default Camera;
