import React from "react";
import { Button } from "../../components/Button";
import { TbPhotoSensor2 } from "react-icons/tb";
import clsx from "clsx";
interface ICapture {
  captureStream?: () => void;
  customerStream:any
}
const Capture = ({
    captureStream,
    customerStream
}: ICapture) => {
  return (
    <Button
      className={clsx("btn-video", {
       
      })}
      onClick={captureStream}
    >
      <TbPhotoSensor2 />
    </Button>
  );
};

export default Capture;
