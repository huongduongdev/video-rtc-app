import React, { useState } from "react";
import { FaMicrophoneSlash, FaMicrophone } from "react-icons/fa";
import { Button } from "../../components/Button";
const Mic: React.FC<{
  localStream: MediaStream;
}> = ({ localStream }) => {
  const [micEnabled, setMicEnabled] = useState(true);

  const handleToggleMic = () => {
    localStream
      .getAudioTracks()
      .forEach((track) => (track.enabled = !track.enabled));
    setMicEnabled(!micEnabled);
  };
  return (
    <Button className='btn-video' onClick={handleToggleMic}>
      {micEnabled ? <FaMicrophone /> : <FaMicrophoneSlash />}
    </Button>
  );
};
export default Mic;
