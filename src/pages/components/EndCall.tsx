import React from 'react'
import { Button } from '../../components/Button'
import {MdCallEnd} from 'react-icons/md'
interface IEncallProps {
  leaveCall?:() => void;

}
const EndCall = ({
  leaveCall
}:IEncallProps) => {
  return (
    <Button className='btn-endcall' onClick={leaveCall}>
        <MdCallEnd />
    </Button>
  )
}

export default EndCall