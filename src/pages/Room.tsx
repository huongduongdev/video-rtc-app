import * as React from "react";
import useVideo from "../hooks/useVideo";
import Camera from "./components/Camera";
import Mic from "./components/Mic";
import EndCall from "./components/EndCall";
import Record from "./components/Record";
import Capture from "./components/Capture";
import CopyClipBoard from "./room/CopyClipBoard";
import HostView from "./room/HostView";
import CustomerView from "./room/CustomerView";
import RecordAudio from "./components/RecordAudio";
import { useLocation } from "react-router-dom";
import FormInviteCustomer from "./room/form-invite-customer";
import { detectMob } from "../ultils/ws";

export const Room = () => {
  
  const {
    leaveCall,
    customerStream,
    stream,
    myVideo,
    callAccepted,
    callEnded,
    userVideo,
    startRecording,
    stopRecording,
    recorder,
    isRoomHost,
    roomId,
    canvasRef,
    captureSnapshot,
    hostName,
    remoteName,
    startRecordingAudio,
    stopRecordingAudio,
    recorderAudio,
    setInviteData,
    inviteData,
    handleInvite,
    inviteLink,
    isMobile
  } = useVideo();

  React.useEffect(() => {
    if (!isRoomHost && !roomId) {
      const siteUrl = window.location.origin;
      window.location.href = siteUrl;
    }
  }, []);
  
  return (
    <div className="video-container">
      <h1 className="hidden sm:block" style={{ textAlign: "center", color: "#fff" }}>VideoRTC</h1>

      {isRoomHost && (
        <>
         {
          !callAccepted && (
            <>
             {inviteLink && <CopyClipBoard inviteLink={inviteLink} /> }
          <FormInviteCustomer 
          inviteData={inviteData}
          setInviteData={setInviteData}
          handleInvite={()=>handleInvite()}
          />
            </>
          )
         }
          <HostView
            stream={stream}
            myVideo={myVideo}
            hostName={hostName}
            callAccepted={callAccepted}
            callEnded={callEnded}
            userVideo={userVideo}
            remoteName={remoteName}
            isMobile={isMobile}
          />
        </>
      )}
      {
        !isRoomHost && (
          <CustomerView 
          stream={stream}
            myVideo={myVideo}
            hostName={hostName}
            callAccepted={callAccepted}
            callEnded={callEnded}
            userVideo={userVideo}
            remoteName={remoteName}
          />
        )
      }
      <div className="video-bottom">
        {
          isRoomHost && (
            <>
            <Capture
              customerStream={customerStream}
              captureStream={captureSnapshot}
            />
            <Record
              stopRecording={() => stopRecording()}
              startRecording={() => startRecording(customerStream)}
              recorder={recorder}
            />
                        <RecordAudio
              stopRecording={() => stopRecordingAudio()}
              startRecording={() => startRecordingAudio(customerStream)}
              recorder={recorderAudio}
            />
            </>
          )
        }
        <Camera localStream={stream} />
        <Mic localStream={stream} />
        <EndCall leaveCall={() => leaveCall()} />
      </div>
       {
        isRoomHost && callAccepted && !callEnded && (
          <canvas
        ref={canvasRef}
        id="capture"
        width={isMobile ? 180*1.77 : 320*1.77}
        height={isMobile ? 320*1.77 : 180*1.77}
      ></canvas>
        )
       }
    </div>
  );
};
