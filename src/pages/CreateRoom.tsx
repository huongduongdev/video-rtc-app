import * as React from 'react'
import { Button } from '../components/Button';
import { useNavigate } from 'react-router-dom';
import useRoomService from '../stores/useRoom';
const CreateRoom = () => {
  const {setIsRoomHost} = useRoomService()
  const navigate = useNavigate()
  React.useEffect(() => {
    setIsRoomHost(false);
  }, []);
  return (
    <div className="form__video w-screen h-screen bg-white rounded-lg">
      <div className="flex flex-col h-full mt-10">
      <h1 className="text-[30px] font-medium mb-2">Video Call App</h1>
        <Button className='rounded-lg w-[320px] h-[40px] bg-[#222] text-white' onClick={()=>{
          navigate("/join-room?host=true");
        }}>Tạo phòng họp</Button>
      </div>
    </div>
    
  )
}

export default CreateRoom