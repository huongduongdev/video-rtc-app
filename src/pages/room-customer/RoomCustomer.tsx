import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import useRoomService from "../../stores/useRoom";
import { getRoomExists } from "../../ultils/api";
import { detectMob, joinRoom } from "../../ultils/ws";
import {isMobile} from 'react-device-detect';
function useQuery() {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}
const RoomCustomer = () => {
  const query = useQuery();
  const { setStream, setRoomId } = useRoomService();
  const username = query.get("name") as string;
  const uuid = query.get("uuid") as string;
  const [errorMessage, setErrorMessage] = React.useState<any>("");
  const navigate = useNavigate();


  const constraints = {
    audio: true,
    video: {
      width: 1280,
      height: 720,
    },
  };

  const initStream = async () => {
    
    
    await navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
      setStream(stream);
    });
  };

  const handlejoinRoom = async () => {
    
    if (!uuid && !username) return setErrorMessage("Bạn chưa nhập mã phòng");
    const responseMessage = await getRoomExists(uuid);

    const { roomExists, full } = responseMessage;

    if (roomExists) {
      if (full) {
        setErrorMessage(
          "Đã quá số lượng người tham gia. Xin vui lòng thử lại sau"
        );
      } else {
        // join a room !
        await initStream();
        setRoomId(uuid);
        joinRoom(username, uuid,isMobile);
        navigate("/room");
      }
    } else {
      setErrorMessage("Mã phòng không được tìm thấy.");
    }
  };

  React.useEffect(() => {
    handlejoinRoom();
  }, []);

  return <div className="form__video w-screen h-screen bg-white rounded-lg">
  <div className="flex flex-col h-full mt-10">
  {errorMessage}</div></div>;
};

export default RoomCustomer;
