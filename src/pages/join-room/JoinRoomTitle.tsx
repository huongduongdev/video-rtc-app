import React from 'react'

const JoinRoomTitle = ({isRoomHost}:any) => {
    const titleText = isRoomHost ? "Tạo phòng họp" : "Tham gia";
  return (
    <h1 className="text-[30px] font-medium mb-2">{titleText}</h1>
  )
}

export default JoinRoomTitle