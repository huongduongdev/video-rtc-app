import * as React from "react";
import useRoomService from "../../stores/useRoom";
import { getRoomExists } from "../../ultils/api";
import { useNavigate } from "react-router-dom";
import { Button } from "../../components/Button";
import { createNewRoom, joinRoom } from "../../ultils/ws";
const JoinRoomContent = ({ isRoomHost }: any) => {
  const { setUserName, setRoomId, setStream } = useRoomService();

  const constraints = {
    audio: true,
    video: {
      width: 1280,
      height: 720,
    },
  };

  const initStream = async () => {
    await navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
      setStream(stream);
    });
  };

  const [roomIdValue, setRoomIdValue] = React.useState("");
  const [nameValue, setNameValue] = React.useState("");
  const [errorMessage, setErrorMessage] = React.useState<any>("");

  const navigate = useNavigate();

  const handleJoinRoom = async () => {
    setUserName(nameValue);
    if (isRoomHost) {
      await initStream();
      createNewRoom(nameValue);
      navigate("/room");
    } else {
      await handlejoinRoom();
    }
  };

  const handlejoinRoom = async () => {
    console.log("id room =====", roomIdValue);
    if (!roomIdValue) return setErrorMessage("Bạn chưa nhập mã phòng");
    const responseMessage = await getRoomExists(roomIdValue);

    const { roomExists, full } = responseMessage;

    if (roomExists) {
      if (full) {
        setErrorMessage(
          "Đã quá số lượng người tham gia. Xin vui lòng thử lại sau"
        );
      } else {
        // join a room !
        await initStream();
        setRoomId(roomIdValue);
        joinRoom(nameValue, roomIdValue,false);
        navigate("/room");
      }
    } else {
      setErrorMessage("Mã phòng không được tìm thấy.");
    }
  };

  return (
    <>
      <div className="form__input__box">
        <input
          className="form__input border rounded"
          placeholder="Nhập tên"
          value={nameValue}
          onChange={(e) => {
            setNameValue(e.target.value);
          }}
        />
      </div>

      {!isRoomHost && (
        <div className="form__input__box ">
          <input
            className="form__input border rounded"
            placeholder="Nhập ID phòng"
            value={roomIdValue}
            onChange={(e) => {
              setRoomIdValue(e.target.value);
            }}
          />
        </div>
      )}

      {errorMessage}
      <div className="join_room_buttons_container">
        <Button
          className="rounded-lg w-[320px] h-[40px] bg-[#222] text-white mb-2"
          onClick={() => handleJoinRoom()}
        >
          {isRoomHost ? "Tạo phòng" : "Tham gia"}
        </Button>
        <Button
          className="rounded-lg w-[320px] h-[40px] bg-[#222] text-white mb-2"
          onClick={() => {
            navigate("/");
          }}
        >
          Quay lại
        </Button>
      </div>
    </>
  );
};

export default JoinRoomContent;
