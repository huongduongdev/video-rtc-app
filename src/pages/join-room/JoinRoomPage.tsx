import React, { useEffect } from 'react'
import { useLocation } from 'react-router-dom';
import useRoomService from '../../stores/useRoom';
import JoinRoomTitle from './JoinRoomTitle';
import JoinRoomContent from './JoinRoomContent';

const JoinRoomPage = () => {
    const search = useLocation().search;
    const {setIsRoomHost, isRoomHost, roomId} = useRoomService()

    useEffect(() => {
        const isRoomHost = new URLSearchParams(search).get("host");
        if (isRoomHost) {
            setIsRoomHost(true);
        }
      }, []);
    
  return (
    <div className="form__video w-screen h-screen bg-white rounded-lg">
    <div className="flex flex-col h-full mt-10">
        <JoinRoomTitle />
        <JoinRoomContent isRoomHost={isRoomHost} roomId={roomId}/>
    </div>
  </div>
  )
}

export default JoinRoomPage