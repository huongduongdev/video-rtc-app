import clsx from "clsx";
import React from "react";
import { detectMob } from "../../ultils/ws";
interface Props {
  stream: MediaStream;
  myVideo: React.RefObject<HTMLVideoElement>;
  hostName: string;
  callAccepted: boolean;
  callEnded: boolean;
  userVideo: React.RefObject<HTMLVideoElement>;
  remoteName: string;
  isMobile:boolean;
}
const HostView = ({
  myVideo,
  hostName,
  callAccepted,
  callEnded,
  userVideo,
  remoteName,
  stream,
  isMobile
}: Props) => {
  return (
    <div className="video-list sm:h-[350px]">
      <div
        className={clsx("video stream-video", {
          ["sm:w-6/12"]: stream,
        })}
      >
        {stream && (
          <>
            <video playsInline muted ref={myVideo} autoPlay />
            <span className="video-name">{hostName}</span>
          </>
        )}
      </div>
      <div
        
        className={clsx("video remote-video", {
          ["sm:w-6/12"]: callAccepted && !callEnded,
        })}
      >
        {callAccepted && !callEnded ? (
          <>
            <video style={{objectFit:isMobile ? 'contain' : 'cover'}} playsInline ref={userVideo} autoPlay />
            <span className="video-name">{remoteName}</span>
          </>
        ) : null}
      </div>
    </div>
  );
};

export default HostView;
