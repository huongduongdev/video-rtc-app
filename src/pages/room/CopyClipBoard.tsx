import * as React from 'react'
import {BiCopy} from 'react-icons/bi'
type IType = {
  inviteLink:string
}
const CopyClipBoard = ({inviteLink}:IType) => {
    const [copyToClipboard] = useCopyToClipboard();

  
  const handleClickCopy = () => {
    // Copy the text from the input field into the clipboard
    copyToClipboard(inviteLink);
  };
  return (
    <div className='form__input__box form__input__copy'>
    <input value={inviteLink} disabled className='form__input border rounded bg-[#fff]' />
      <button className='btn__copyclipboard' onClick={handleClickCopy}>
        <BiCopy />
      </button>
    </div>
  )
}

export default CopyClipBoard

function useCopyToClipboard() {
    
  
    const copy = async (text: string) => {
        await navigator.clipboard.writeText(text);
    };
  
    // 👇 We want the result as a tuple
    return [copy] as const;
  }