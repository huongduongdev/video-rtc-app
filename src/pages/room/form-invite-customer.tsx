import React from 'react'
import { FormInput } from '../../components/FormInput'
import { Button } from '../../components/Button';
interface Props{
    setInviteData:any;
    inviteData:string,
    handleInvite:()=>void;
}
const FormInviteCustomer = ({
    handleInvite,setInviteData,inviteData
}:Props) => {
  return (
    <div className='form__invite'>
        <h3 className='text-[20px] text-medium mb-2'>Thông tin khách hàng</h3>
        <FormInput 
            value={inviteData}
            classnamegroup='form__input__group'
            classnameinput='form__input rounded border border-solid border-[#ced4da]'
            label='Số điện thoại:'
            placeholder='Nhập số điện thoại'
            type='text'
            onChange={(e)=>{
                setInviteData(e.target.value)
            }}
        />
        <Button className='btn__invite' onClick={handleInvite}>Gửi tin nhắn</Button>
    </div>
  )
}

export default FormInviteCustomer