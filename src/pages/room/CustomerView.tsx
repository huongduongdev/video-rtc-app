import clsx from "clsx";
import React from "react";
interface Props {
  stream: MediaStream;
  myVideo: React.RefObject<HTMLVideoElement>;
  hostName: string;
  callAccepted: boolean;
  callEnded: boolean;
  userVideo: React.RefObject<HTMLVideoElement>;
  remoteName: string;
}
const CustomerView = ({
  myVideo,
  hostName,
  callAccepted,
  callEnded,
  userVideo,
  remoteName,
  stream,
}: Props) => {
  return (
    <div className="video-list">
      <div
        className={clsx("video stream-video", {
          ["sm:w-6/12"]: stream,
        })}
      >
        {stream && (
          <>
            <video playsInline muted ref={myVideo} autoPlay />
            <span className="video-name">{remoteName}</span>
          </>
        )}
      </div>
      <div
        className={clsx("video stream-video", {
          ["sm:w-6/12"]: callAccepted && !callEnded,
        })}
      >
        {callAccepted && !callEnded ? (
          <>
            <video playsInline ref={userVideo} autoPlay />
            <span className="video-name">{hostName}</span>
          </>
        ) : null}
      </div>
    </div>
  );
};

export default CustomerView;
