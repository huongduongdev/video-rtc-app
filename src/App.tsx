import * as React from 'react';
import logo from './logo.svg';
import './App.scss';
import { Room } from './pages/Room';
import { Routes, Route, BrowserRouter } from "react-router-dom"
import CreateRoom from './pages/CreateRoom';
import JoinRoomPage from './pages/join-room/JoinRoomPage';
import Layout from './pages/Layout';
import useRoomService from './stores/useRoom';
import { socket } from './ultils/ws';
import RoomCustomer from './pages/room-customer/RoomCustomer';

function App() {
  const {setRoomId,setParticipants,setStream}=useRoomService()
  React.useEffect(() => {
    socket.on("connect", () => {
      console.log("successfully connected with socket io server");
      console.log(socket.id);
    });
    
    socket.on("room-id", (data) => {
      const { roomId } = data;
      setRoomId(roomId)
      
    });


    socket.on("room-update", (data) => {
      const { connectedUsers } = data;
      console.log('connectedUsers ===========',connectedUsers);
      setParticipants(connectedUsers)
    });


  }, [])

  return (
    <BrowserRouter>
       <Routes>
         <Route element={<Layout />}>
           <Route path="/" element={<CreateRoom />} />
           <Route path="/join-room" element={<JoinRoomPage />} />
           <Route path="/room" element={<Room />} />
           <Route path="/customer/" element={<RoomCustomer />} />
         </Route>
       </Routes>
     </BrowserRouter>
  );
}

export default App;
