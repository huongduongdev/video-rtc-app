import * as React from "react";
export const Button = React.forwardRef<
  HTMLButtonElement,
  React.PropsWithChildren<React.ButtonHTMLAttributes<HTMLButtonElement>>
>(function Button(props, ref) {
  return (
    <button ref={ref} {...props}>
      {props.children}
    </button>
  );
});