import * as React from "react";
import { HTMLInputTypeAttribute,ChangeEvent } from "react";
interface Props  {
  props?: any;
  onBlur?: (event: ChangeEvent<HTMLInputElement>) => void;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  label?:string,
  error?:string,
  classnameinput?:string,
  classnamegroup?:string,
  placeholder?:string,
  type?:HTMLInputTypeAttribute,
  value?:string,
}
export const FormInput = React.forwardRef<HTMLInputElement, Props>(
  function TextInput(
    {  onChange, ...props },
    forwardedRef
  ) {

  
  return (
    props.classnamegroup ? (
        <div className="form__input__group">
            <input className={props.classnameinput} placeholder={props.placeholder} value={props.value} {...props} onChange={onChange} ref={forwardedRef} />
            {props.error && <span className="block text-red-500 text-left mt-1">{props.error}</span>}
        </div>
    ) : (
        <>
         <input className={props.classnameinput} placeholder={props.placeholder} value={props.value} {...props} onChange={onChange} ref={forwardedRef} />
        {props.error && <span className="block text-red-500 text-left mt-1">{props.error}</span>}
        </>
    )
  );
})