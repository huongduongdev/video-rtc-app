import React from "react";
import { create } from "zustand";
export type IParticipantItem = {
  id?: string;
  identity?: string;
  roomId: string;
  socketId: string;
  isHost?:boolean,
  isMobile:boolean,
};
interface RoomState {
  userName: string;
  isRoomHost: boolean;
  roomId: string;
  participants: IParticipantItem[];
  stream?:any;
  myRemote?:any;
  callAccepted?:boolean
}
const initialState = {
  userName: "",
  isRoomHost: false,
  participants: [],
  roomId:'',
  callAccepted:false,
};

const roomStore = create<RoomState>(() => initialState);

interface IRoomService extends RoomState {
  setUserName: (userName: string) => void;
  setRoomId: (roomId: string) => void;
  setIsRoomHost: (isRoomHost?: boolean) => void;
  setParticipants:(list: IParticipantItem[]) => void;
  resetState: () => void;
  setStream: (stream: any) => void;
  setRemoteStream: (stream: any) => void;
}

export default function useRoomService(): IRoomService {
  const { isRoomHost, roomId, participants, userName,stream ,myRemote,callAccepted } = roomStore();
  return {
    userName,
    isRoomHost,
    roomId,
    participants,
    stream,
    setIsRoomHost: (isRoomHost) => {
      roomStore.setState({
        isRoomHost: isRoomHost,
      });
    },
    setUserName: (username) => {
      roomStore.setState({
        userName: username,
      });
    },
    setRoomId: (roomId) => {

      roomStore.setState({
        roomId: roomId,
      });
    },
    setParticipants:(list) =>{
      roomStore.setState({
        participants: list,
      });
    },
    setStream: (stream) => {
      roomStore.setState({
        stream: stream,
      });
    },
    
    setRemoteStream: (stream) => {
      roomStore.setState({
        myRemote: stream,
      });
    },
    resetState: () => {},
  };
}
