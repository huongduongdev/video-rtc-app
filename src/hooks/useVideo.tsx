import { useEffect, useRef, useState } from "react";
import { socket } from "../ultils/ws";
import Peer from "simple-peer";
import { saveAs } from "file-saver";
import useRoomService, { IParticipantItem } from "../stores/useRoom";
import RecordRTC, {
  MultiStreamRecorder,
  StereoAudioRecorder,
} from "recordrtc";

const peers: any = {};

const useVideo = () => {
  const {
    isRoomHost,
    roomId,
    participants,
    stream,
    setStream,
    setRemoteStream,
    myRemote,
  } = useRoomService();
  //   console.log("user listing", participants);

  //   console.log("ishost ===========", isRoomHost);
  //   console.log("userName ===========", userName);
  //   console.log("roomId ===========", roomId);
  const [inviteData, setInviteData] = useState<string>("");
  const [inviteLink,setInviteLink] = useState<string>('')
  const [customerStream, setCustomerStream] = useState<any>();
  const [callAccepted, setCallAccepted] = useState<boolean>(false);
  const [idToCall, setIdToCall] = useState<any>("");
  const [callEnded, setCallEnded] = useState<boolean>(false);
  const [name, setName] = useState<any>("");
  const [recorder, setRecorder] = useState<any>(null);
  const [recorderAudio, setRecorderAudio] = useState<any>(null);
  const myVideo = useRef<HTMLVideoElement>(null);
  const userVideo = useRef<HTMLVideoElement>(null);
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const connectionRef = useRef<any>(null);
  const [dateStarted, setDateStart] = useState<any>(null);
  const [remote, setRemote] = useState<any>();

  const peerConfiguration = () => {
    const turnIceServers = null;

    if (turnIceServers) {
      // TODO use TURN server credentials
    } else {
      console.warn("Using only STUN server");
      return {
        iceServers: [
          {
            urls: "stun:stun.l.google.com:19302",
          },
        ],
      };
    }
  };

  useEffect(() => {
    if (stream && myVideo.current) {
      myVideo.current.srcObject = stream;
    }
  }, [stream]);

  useEffect(() => {
    socket.on("conn-signal", (data) => {

      peers[data.connUserSocketId].signal(data.signal);
      connectionRef.current = peers[data.connUserSocketId];
    });

    socket.on("conn-prepare", (data) => {
      const { connUserSocketId } = data;
      prepareNewPeerConnection(connUserSocketId, false);
      // inform the user which just join the room that we have prepared for incoming connection
      //User 2 will send back conn-init to User 1 through Server
      socket.emit("conn-init", { connUserSocketId: connUserSocketId });
    });
    socket.on("conn-init", (data) => {
      // This is User 1 and will call peer connection on this side
      // it will pass isInitiatiot = true now,
      // since this is the client which started the calls
      console.log("conn init =============", data);
      const { connUserSocketId } = data;
      prepareNewPeerConnection(connUserSocketId, true);
    });
    socket.on("invite-link", (data) => {
      const hostname = location.hostname
      
      if(hostname === 'localhost'){
        setInviteLink(data?.inviteLocalUrl)
      } else {
        setInviteLink(data?.inviteUrl)
      }
      
      
    });
  }, []);

  useEffect(() => {
    if (customerStream) {
      socket.on("user-disconnected", (data) => {
        console.log("user discouneect =============", data);

        // destroy_socket_event()
        removePeerConnection(data);
      });
    }
  }, [customerStream]);

  const prepareNewPeerConnection = (
    connUserSocketId: any,
    isInitiator: boolean
  ) => {
    peers[connUserSocketId] = new Peer({
      initiator: isInitiator,
      trickle: false,
      stream: stream,
      // config:peerConfiguration()
    });

    peers[connUserSocketId].on("signal", (data: any) => {
      // webRTC offer, webRTC Answer (SDP informations), ice candidates
      const signalData = {
        signal: data,
        connUserSocketId: connUserSocketId,
      };
      console.log("data", data);

      socket.emit("conn-signal", signalData);
    });
    peers[connUserSocketId].on("stream", (stream: any) => {
      setCallAccepted(true);
      console.log("new stream came =========", stream);
      if (userVideo.current && stream) userVideo.current.srcObject = stream;
      setCustomerStream(stream);
      console.log('customer stream resolutuion ========',stream.getVideoTracks()[0].getSettings().height,
      stream.getVideoTracks()[0].getSettings().width);
      
      // addStream(stream, connUserSocketId);
      // streams = [...streams, stream];
      // console.log("streams listing ==========", streams);
    });
  };

  const handleInvite = () => {
    const dataEmit = {
      identity:inviteData,
      roomId: participants[0].roomId,
      
    };


    socket.emit("invite-user", dataEmit);
  };

  const removePeerConnection = (data: any) => {
    console.log("data list", participants);
    const { socketId } = data;

    const matchHost = participants.find((v) => v.socketId === socketId);

    console.log("matchHost", matchHost);
    if (userVideo.current) {
      customerStream.getTracks().forEach(function (track: any) {
        track.stop();
      });
      userVideo.current.srcObject = null;
    }

    setCallAccepted(false);
    setCallEnded(true);
    if (matchHost && !isRoomHost) {
      peers[matchHost.socketId].destroy();
      delete peers[matchHost.socketId];
      leaveCall();
    }
    if (peers[socketId]) {
      peers[socketId].destroy();
    }
    delete peers[socketId];
  };

  const leaveCall = () => {
    const siteUrl = window.location.origin;
    window.location.href = siteUrl;
  };

  // stop record
  const stopRecording = async () => {
    if (recorder) {
      console.log("================recorder", recorder);
      recorder.stop(function (blob: any) {
        const newBlob = recorder.blob;

        console.log("==========blob", newBlob);
        setRecorder(null);
        const mp4File = new File([newBlob], "demo.mp4", { type: "video/mp4" });
        saveAs(mp4File, `Video-${Date.now()}.mp4`);
      });
    }
  };
  // start record
  const startRecording = async (recorder: any) => {
    if (recorder) {
      const recorder = new MultiStreamRecorder([customerStream, stream], {
        type: "video",
        mimeType: "video/mp4",
      });

      await recorder.record();

      setRecorder(recorder);
      console.log("recorder RecordRTC =======", recorder);
    }
  };

  // record audio

  const startRecordingAudio = async (recorder: any) => {
    if (recorder) {
      const recorder = new RecordRTC(customerStream, {
        type: "audio",
        recorderType: StereoAudioRecorder,
      });

      await recorder.startRecording();

      setRecorderAudio(recorder);
      console.log("recorder RecordRTC =======", recorder);
    }
  };

  const stopRecordingAudio = async () => {
    if (recorderAudio) {
      console.log("================recorder", recorderAudio);
      recorderAudio.stopRecording(function () {
        // const newBlob = recorderAudio.blob;

        // console.log("==========blob", newBlob);
        // setRecorderAudio(null);
        // const mp4File = new File([newBlob], "demo.mp3", { type: "audo/mp3" });
        // saveAs(mp4File, `Mp3-${Date.now()}.mp3`);
        setRecorderAudio(null);
        const blob = recorderAudio.getBlob();

        // open recorded blob in a new window
        window.open(URL.createObjectURL(blob));
        console.log("blob", blob);
      });
    }
  };

  useEffect(() => {
    if (recorder) {
      const dateStarted = new Date().getTime();
      const intervalId = setInterval(() => {
        console.log(
          "Recording Duration: ",
          calculateTimeDuration((new Date().getTime() - dateStarted) / 1000)
        );
      }, 1000);
      return () => {
        clearInterval(intervalId);
      };
    }
  }, [recorder]);

  //   time record

  function calculateTimeDuration(secs: any) {
    const hr: any = Math.floor(secs / 3600);
    let min: any = Math.floor((secs - hr * 3600) / 60);
    let sec: any = Math.floor(secs - hr * 3600 - min * 60);

    if (min < 10) {
      min = "0" + min;
    }

    if (sec < 10) {
      sec = "0" + sec;
    }

    if (hr <= 0) {
      return min + ":" + sec;
    }

    return hr + ":" + min + ":" + sec;
  }
  // capture

  const captureSnapshot = () => {
    if (!userVideo.current) return null;
    const canvas = canvasRef.current;
    const video = userVideo.current;

    if (video) {
      const img: any = new Image();
      img.src = canvas?.toDataURL("image/png");
      canvas
        ?.getContext("2d")
        ?.drawImage(video, 0, 0, canvas.width, canvas.height);
      console.log("img", img.src);
    }
  };
 

  return {
    leaveCall,
    customerStream,
    stream,
    myVideo,
    callAccepted,
    callEnded,
    userVideo,
    name,
    setName,
    idToCall,
    setIdToCall,
    startRecording,
    stopRecording,
    startRecordingAudio,
    stopRecordingAudio,
    recorder,
    recorderAudio,
    roomId,
    isRoomHost,
    remote,
    canvasRef,
    captureSnapshot,
    hostName: participants[0]?.identity || "No name",
    remoteName: participants[1]?.identity || "No name",
    isMobile:participants[1]?.isMobile,
    setInviteData,
    inviteData,
    handleInvite,
    inviteLink,
  };
};
export default useVideo;
